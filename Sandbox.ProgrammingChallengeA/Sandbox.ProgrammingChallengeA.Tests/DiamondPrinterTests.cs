﻿using Sandbox.ProgrammingChallengeA.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using System.IO;
using System.Text;

namespace Sandbox.ProgrammingChallengeA.Tests {
  public class DiamondPrinterTests {
    [Fact]
    public void EmptyDiamond() {
      var lines = new List<String>() { };
      this._TestPrintDiamond(0, lines);
    }
    [Fact]
    public void SingleDiamond() {
      var lines = new List<String>() {
        "x"
      };
      this._TestPrintDiamond(1, lines);
    }
    [Fact]
    public void DoubleDiamond() {
      var lines = new List<String>() {
        "xx",
        "xx"
      };
      this._TestPrintDiamond(2, lines);
    }
    [Fact]
    public void OddDiamond() {
      var lines = new List<String>() {
        " x",
        "xxx",
        " x"
      };
      this._TestPrintDiamond(3, lines);
    }
    [Fact]
    public void EvenDiamond() {
      var lines = new List<String>() {
        " xx",
        "xxxx",
        "xxxx",
        " xx"
      };
      this._TestPrintDiamond(4, lines);
    }
    private void _TestPrintDiamond(int size, IEnumerable<string> lines) {
      var delim = "\r\n";
      var sb = new StringBuilder();
      var sw = new StringWriter(sb);
      var dp = new DiamondPrinter();
      dp.PrintDiamond(sw, size);
      var output = sb.ToString().Trim("\r\n".ToCharArray());
      var expected = String.Join(delim, lines.ToArray());
      Assert.Equal(expected, output);
    }

  }
}

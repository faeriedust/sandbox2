﻿using Sandbox.Libraries.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Sandbox.ProgrammingChallengeA.Logic {
  public class DiamondPrinter {
    public async void PrintDiamond(TextWriter tw, int size) {
      foreach (var line in _GetLines(size)) {
        await tw.WriteLineAsync(line);
      }
    }
    private IEnumerable<string> _GetLines(int size) {

      int bigHalfWidth = size - (size / 2);
      int smallHalfWidth = size / 2;

      int startingXCount = size % 2 == 0 ? 2 : 1;
      int startingSpaceCount = bigHalfWidth - 1;

      var spaces = SandboxHelpers.Range(bigHalfWidth - 1, bigHalfWidth, -1)
        .Concat(SandboxHelpers.Range(size % 2, smallHalfWidth))
        .Select(n => SandboxHelpers.Repeat(" ", n));

      var xs = SandboxHelpers.Range(startingXCount, bigHalfWidth, 2)
        .Concat(SandboxHelpers.Range(size - 2 * (size % 2), smallHalfWidth, -2))
        .Select(n => SandboxHelpers.Repeat("x", n));

      var lines = spaces
        .Zip(xs, Tuple.Create)
        .Select(pair => $"{pair.Item1}{pair.Item2}");

      return lines;
    }
  }
}
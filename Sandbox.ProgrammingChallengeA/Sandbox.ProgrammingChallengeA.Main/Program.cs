﻿using Sandbox.ProgrammingChallengeA.Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Sandbox.ProgrammingChallengeA.Main {
  public class Program {
    public static void Main(string[] args) {
      var sw = new StreamWriter(Console.OpenStandardOutput());
      sw.AutoFlush = true;
      Console.SetOut(sw);
      var dp = new DiamondPrinter();
      dp.PrintDiamond(sw, 6);
      Console.Read();
    }
  }
}

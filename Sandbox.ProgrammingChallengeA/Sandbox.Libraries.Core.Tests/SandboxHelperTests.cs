﻿using Sandbox.Libraries.Core;
using System.Linq;
using Xunit;

namespace Sandbox.Libraries.Tests {
  // This project can output the Class library as a NuGet Package.
  // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
  public class SandboxHelperTests {
    [Fact]
    public void RepeatTest() {
      Assert.Equal(SandboxHelpers.Repeat("x", 5), "xxxxx");
    }
    [Fact]
    public void RangeTest() {
      CustomAssert.SequenceEqual(Enumerable.Range(1, 5), SandboxHelpers.Range(1, 5));
    }
    [Fact]
    public void ReverseRangeTest() {
      CustomAssert.SequenceEqual(Enumerable.Range(1, 5).Reverse(), SandboxHelpers.Range(5, 5, -1));
    }
  }
}

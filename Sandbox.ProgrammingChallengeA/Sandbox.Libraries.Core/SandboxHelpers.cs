﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sandbox.Libraries.Core {
  public static class SandboxHelpers {
    public static string Repeat(string s, int reps) {
      return String.Concat(Enumerable.Repeat(s, reps));
    }
    /// <summary>
    /// Returns a sequence of elements starting at specified value and incrementing by set amount.
    /// </summary>
    /// <param name="start">the first number to return</param>
    /// <param name="count">the number of elements to return</param>
    /// <param name="step">the amount to step by</param>
    /// <returns></returns>
    public static IEnumerable<int> Range(int start, int count, int step = 1) {
      while (count > 0) {
        yield return start;
        start += step;
        count--;
      }
    }
  }
}

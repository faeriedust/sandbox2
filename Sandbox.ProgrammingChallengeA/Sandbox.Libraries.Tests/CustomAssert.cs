﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Sandbox.Libraries.Tests {
  public static class CustomAssert {
    public static void SequenceEqual<T>(IEnumerable<T> expected, IEnumerable<T> actual) {
      Assert.Equal(expected.Count(), actual.Count());
      foreach (var pair in expected.Zip(actual, Tuple.Create)) {
        Assert.Equal(pair.Item1, pair.Item2);
      }
    }
  }
}
